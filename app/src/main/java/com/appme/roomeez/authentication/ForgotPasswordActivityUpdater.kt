package com.appme.roomeez.authentication

import com.appme.roomeez.SnackbarMessageType

interface ForgotPasswordActivityUpdater {

    /**
     * Triggers a super call to display a toast message
     */
    fun displayToast(message: Int, length: Int)

    fun killActivity()

    /**
     * Triggers a warning to the user if there is something wrong with the email that they entered.
     */
    fun displayEmailError(errorMessage: Int)

    /**
     * Shows the progres bar and disables other views until backend processes complete
     */
    fun displayProgressBar()

    /**
     * Hides the progress bar and enables other views for the user to interact with!
     */
    fun hideProgressBar()

    /**
     * Triggers a super call to display a snackbar message
     */
    fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType)

}