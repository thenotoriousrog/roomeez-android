package com.appme.roomeez.authentication

import com.appme.roomeez.SnackbarMessageType

/**
 * Used to separate the activity from the SignupActivityPresenter and to delegate responses from backend systems
 */
interface SignupActivityUpdater {

    /**
     * There was a problem with the user's email. The appropriate warning needs to be displayed for the user to fix it appropriately
     * @param errorMessage - the error message from strings.xml that we want to display to the user
     */
    fun displayEmailError(errorMessage: Int)

    /**
     * There was a problem with the user's password
     * @param errorMessage - the error message from strings.xml that we want to display to the user
     * @param passwordsMismatch - true if the confirmed password does not match originally entered password, false if they do.
     */
    fun displayPasswordError(errorMessage: Int, passwordsMismatch: Boolean)

    /**
     * Displays the progress bar and also disables all other UI elements from being used while waiting for backend processes to finish
     */
    fun displayProgressBar()

    /**
     * Hides progress bar and enables UI elements to be used again
     */
    fun hideProgressBar()

    /**
     * Displays a snackbar with a message that gives the users more information
     * @param message - the message to display to users
     * @param length - how long to display the snackbar message i.e. LENGTH_SHORT or LENGTH_LONG
     * @param messageType - the type of message we want to display to the user
     */
    fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType)

    fun launchHomeActivity()
}