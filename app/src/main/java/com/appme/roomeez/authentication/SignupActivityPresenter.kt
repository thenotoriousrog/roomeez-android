package com.appme.roomeez.authentication

import android.util.Log
import androidx.core.util.PatternsCompat
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.appme.roomeez.backend.persistance.FirebaseHelper
import com.appme.roomeez.backend.persistance.listeners.FirebaseListener
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

/**
 * This class is responsible for responding to user actions and communicating with the backend to complete the sign in process!
 */
class SignupActivityPresenter(private val updater: SignupActivityUpdater) {

    private val TAG = "SignupActivityPresenter"

    private fun isValidPassword(password: String): Boolean {
        val specialCharPattern = Pattern.compile("[^a-z0-9]", Pattern.CASE_INSENSITIVE)
        val upperCasePattern = Pattern.compile("[A-Z ]")
        val lowerCasePattern = Pattern.compile("[a-z ]")
        val digitPattern = Pattern.compile("[0-9 ]")

        if(password.length < 8) {
            Log.w(TAG, "password has length less than 8!")
            return false
        }
        if(!specialCharPattern.matcher(password).find()) {
            Log.w(TAG, "password does not contain a special character!")
            return false
        }
        if(!upperCasePattern.matcher(password).find()) {
            Log.w(TAG, "password does not contain an uppercase character!")
            return false
        }
        if(!lowerCasePattern.matcher(password).find()) {
            Log.w(TAG, "password does not contain a lowercase character!")
            return false
        }
        if(!digitPattern.matcher(password).find()) {
            Log.w(TAG, "password does not contain a number!")
            return false
        }

        return true
    }

    fun attemptSignUp(email: String, password: String, confirmedPassword: String) {
        updater.displayProgressBar()

        if(email.isBlank()) {
            updater.displayEmailError(R.string.email_empty)
            updater.hideProgressBar()
            return
        }

        if(!PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) { // password is blank and/or doesn't match a real email, throw an error
            updater.displayEmailError(R.string.invalid_email)
            updater.hideProgressBar()
            return
        }

        if(password.isBlank()) {
            updater.displayPasswordError(R.string.password_empty, false)
            updater.hideProgressBar()
            return
        }

        if(!isValidPassword(password)) {
            updater.displayPasswordError(R.string.password_invalid, false)
            updater.hideProgressBar()
            return
        }

        if(password != confirmedPassword) {
            updater.displayPasswordError(R.string.password_confirm_error, true)
            updater.hideProgressBar()
            return
        }

        FirebaseHelper.instance.signupUser(email, password, object : FirebaseListener {
            override fun onCancel() {
                updater.hideProgressBar()
            }

            override fun onError() {
                updater.hideProgressBar()
                updater.displaySnackbar(R.string.firebase_error, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
            }

            override fun onFail() {
                updater.hideProgressBar()
                updater.displaySnackbar(R.string.create_account_error, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
            }

            override fun onSuccess() {
                updater.hideProgressBar()
                updater.launchHomeActivity()
            }
        })

    }

}