package com.appme.roomeez.authentication

import android.widget.Toast
import androidx.core.util.PatternsCompat
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.appme.roomeez.backend.persistance.FirebaseHelper
import com.appme.roomeez.backend.persistance.listeners.FirebaseListener
import com.google.android.material.snackbar.Snackbar

class ForgotPasswordActivityPresenter(private val updater: ForgotPasswordActivityUpdater) {

    fun attemptPasswordRecovery(email: String?) {

        updater.displayProgressBar()

        if(email.isNullOrBlank()) {
            updater.displayEmailError(R.string.email_empty)
            updater.hideProgressBar()
            return
        }

        if(!PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) {
            updater.displayEmailError(R.string.invalid_email)
            updater.hideProgressBar()
            return
        }

        FirebaseHelper.instance.sendPasswordResetEmail(email, object : FirebaseListener {
            override fun onCancel() { updater.hideProgressBar() }

            override fun onFail() {
                updater.displaySnackbar(R.string.send_reset_password_email_error, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
                updater.hideProgressBar()
            }

            override fun onError() {
                updater.displaySnackbar(R.string.send_reset_password_email_error, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
                updater.hideProgressBar()
            }

            override fun onSuccess() {
                updater.displayToast(R.string.send_reset_password_email_success, Toast.LENGTH_LONG)
                updater.killActivity()
            }
        })
    }
}