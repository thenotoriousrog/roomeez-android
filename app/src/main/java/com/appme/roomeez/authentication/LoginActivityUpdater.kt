package com.appme.roomeez.authentication

import android.content.Context
import com.appme.roomeez.SnackbarMessageType

interface LoginActivityUpdater {

    /**
     * There was a problem with the user's email. The appropriate warning needs to be displayed for the user to fix it appropriately
     * @param errorMessage - the error message from strings.xml that we want to display to the user
     */
    fun displayEmailError(errorMessage: Int)

    /**
     * There was a problem with the user's password
     * @param errorMessage - the error message from strings.xml that we want to display to the user
     */
    fun displayPasswordError(errorMessage: Int)

    /**
     * Shows the progres bar and disables other views until backend processes complete
     */
    fun displayProgressBar()

    /**
     * Hides the progress bar and enables other views for the user to interact with!
     */
    fun hideProgressBar()

    /**
     * Displays a snackbar with a message that gives the users more information
     * @param message - the message to display to users
     * @param length - how long to display the snackbar message i.e. LENGTH_SHORT or LENGTH_LONG
     * @param messageType - the type of message we want to display to the user
     */
    fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType)

    fun launchHomeActivity()
}