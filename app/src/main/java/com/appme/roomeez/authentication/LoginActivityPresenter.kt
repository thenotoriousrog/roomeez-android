package com.appme.roomeez.authentication

import android.content.Intent
import android.util.Log
import androidx.core.util.PatternsCompat
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.appme.roomeez.backend.persistance.FirebaseHelper
import com.appme.roomeez.backend.persistance.listeners.FirebaseListener
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.GoogleAuthProvider

class LoginActivityPresenter(private val updater: LoginActivityUpdater) {

    private val TAG = "LoginActivityPresenter"

    fun attemptAutoLogin() {
        if(FirebaseHelper.instance.isLoggedIn()) {
            updater.launchHomeActivity()
        }
    }

    /**
     * Attempts a standard login i.e. with their own email and password
     */
    fun attemptLogin(email: String?, password: String?) {
        updater.displayProgressBar()

        if(email.isNullOrBlank()) {
            updater.hideProgressBar()
            updater.displayEmailError(R.string.email_empty)
            return
        }

        if(!PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()) { // password is blank and/or doesn't match a real email, throw an error
            updater.displayEmailError(R.string.invalid_email)
            updater.hideProgressBar()
            return
        }

        if(password.isNullOrBlank()) {
            updater.hideProgressBar()
            updater.displayPasswordError(R.string.password_empty)
            return
        }

        FirebaseHelper.instance.loginUser(email, password, object : FirebaseListener {
            override fun onCancel() {
                updater.hideProgressBar()
            }

            override fun onError() {
                updater.hideProgressBar()
                updater.displaySnackbar(R.string.error_logging_in, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
            }

            override fun onFail() {
                updater.hideProgressBar()
                updater.displaySnackbar(R.string.error_logging_in, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
            }

            override fun onSuccess() {
                updater.hideProgressBar()
                updater.launchHomeActivity()
            }
        })
    }

    fun attemptLoginWithGoogle(data: Intent?) {
        val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data) // **Note: The task from this action is always completed. No listener is needed.
        try {
            val account = task.getResult(ApiException::class.java)
            val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
            FirebaseHelper.instance.loginUserWithGoogle(credential, object : FirebaseListener {
                override fun onCancel() { updater.hideProgressBar() }

                override fun onError() {
                    updater.hideProgressBar()
                    updater.displaySnackbar(R.string.login_with_google_error, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
                }

                override fun onFail() {
                    updater.hideProgressBar()
                    updater.displaySnackbar(R.string.login_with_google_fail, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
                }

                override fun onSuccess() {
                    updater.hideProgressBar()
                    updater.launchHomeActivity()
                }
            })

        } catch (ex: ApiException) {
            Log.w(TAG, "Caught exception while trying to access google account!", ex)
            updater.hideProgressBar()
            updater.displaySnackbar(R.string.login_with_google_exception, Snackbar.LENGTH_LONG, SnackbarMessageType.ERROR)
        }
    }
}