package com.appme.roomeez.authentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.home.HomeActivity
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : BaseActivity(), LoginActivityUpdater {

    private val presenter = LoginActivityPresenter(this)
    private lateinit var container: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var emailLayout: TextInputLayout
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var forgotPasswordButton: MaterialButton
    private lateinit var loginButton: MaterialButton
    private lateinit var loginWithGoogleButton: MaterialButton
    private lateinit var signupButton: MaterialButton
    private val GOOGLE_SIGNIN_RC = 1000 // Reponse code for the google sign in task

    private lateinit var googleSignInOptions: GoogleSignInOptions
    private lateinit var googleSignInClient: GoogleSignInClient

    private fun prepareGoogleSignInConfig() {
        this.googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.roomeez_web_client_id))
            .requestEmail()
            .build()

        this.googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions)
    }

    override fun getLayoutResId(): Int {
        return R.layout.login_activity
    }

    override fun getSnackbarContainerView(): View {
        return container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        prepareGoogleSignInConfig()

        supportActionBar?.title = getString(R.string.login_to_roomeez)

        this.container = findViewById(R.id.login_activity_container)
        this.progressBar = findViewById(R.id.login_progress_bar)
        this.emailLayout = findViewById(R.id.login_enter_email_layout)
        this.passwordLayout = findViewById(R.id.login_enter_password_layout)
        this.forgotPasswordButton = findViewById(R.id.forgot_password_button)
        this.forgotPasswordButton.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
        this.loginButton = findViewById(R.id.login_button)
        this.loginButton.setOnClickListener {
            emailLayout.isErrorEnabled = false
            passwordLayout.isErrorEnabled = false
            presenter.attemptLogin(emailLayout.editText?.text.toString(), passwordLayout.editText?.text.toString())
        }
        this.loginWithGoogleButton = findViewById(R.id.google_sign_in_button)
        this.loginWithGoogleButton.setOnClickListener {
            val signInWithGoogleIntent = googleSignInClient.signInIntent
            startActivityForResult(signInWithGoogleIntent, GOOGLE_SIGNIN_RC)
        }
        this.signupButton = findViewById(R.id.sign_up_button)
        this.signupButton.setOnClickListener { startActivity(Intent(this, SignupActivity::class.java)) }
    }

    override fun onStart() {
        super.onStart()
        // TODO: Might be good to come up with a splash screen once we start getting custom logos and icons and what not!
        presenter.attemptAutoLogin() // **Note: If this is successful, the user will be logged in and the login activity is killed before showing the login screen
    }

    private fun disableLayouts() {
        this.loginButton.isClickable = false
        this.forgotPasswordButton.isClickable = false
        this.loginWithGoogleButton.isClickable = false
        this.signupButton.isClickable = false
    }

    private fun enableLayouts() {
        this.loginButton.isClickable = true
        this.forgotPasswordButton.isClickable = true
        this.loginWithGoogleButton.isClickable = true
        this.signupButton.isClickable = true
    }

    override fun displayEmailError(errorMessage: Int) {
        emailLayout.error = getString(errorMessage)
        emailLayout.isErrorEnabled = true
    }

    override fun displayPasswordError(errorMessage: Int) {
        passwordLayout.error = getString(errorMessage)
        passwordLayout.isErrorEnabled = true
    }

    override fun displayProgressBar() {
        this.progressBar.visibility = View.VISIBLE
        disableLayouts()
    }

    override fun hideProgressBar() {
        this.progressBar.visibility = View.GONE
        enableLayouts()
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType) {
        super.displaySnackbarMessage(message, length, messageType)
    }

    override fun launchHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GOOGLE_SIGNIN_RC) { // this request code is what we need from google
            presenter.attemptLoginWithGoogle(data)
        }
    }
}