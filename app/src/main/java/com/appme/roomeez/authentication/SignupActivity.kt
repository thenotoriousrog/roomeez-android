package com.appme.roomeez.authentication

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.home.HomeActivity
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

/**
 * This activity is used for when new user's sign up to join roomeez!
 */
class SignupActivity : BaseActivity(), SignupActivityUpdater {

    private val presenter = SignupActivityPresenter(this)
    private lateinit var container: ConstraintLayout
    private lateinit var emailLayout: TextInputLayout
    private lateinit var passwordLayout: TextInputLayout
    private lateinit var confirmPasswordLayout: TextInputLayout
    private lateinit var signupButton: MaterialButton
    private lateinit var progressBar: ProgressBar

    override fun getLayoutResId(): Int {
        return R.layout.signup_activity
    }

    override fun getSnackbarContainerView(): View {
        return container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.sign_up_for_roomeez)

        container = findViewById(R.id.signup_activity_container)
        emailLayout = findViewById(R.id.login_enter_email_layout)
        passwordLayout = findViewById(R.id.login_enter_password_layout)
        confirmPasswordLayout = findViewById(R.id.confirm_enter_password_layout)
        progressBar = findViewById(R.id.progress_bar)
        signupButton = findViewById(R.id.sign_up_button)
        signupButton.setOnClickListener {
            emailLayout.isErrorEnabled = false
            passwordLayout.isErrorEnabled = false
            confirmPasswordLayout.isErrorEnabled = false
            presenter.attemptSignUp(emailLayout.editText?.text.toString(), passwordLayout.editText?.text.toString(), confirmPasswordLayout.editText?.text.toString())
        }
    }

    override fun displayEmailError(errorMessage: Int) {
        emailLayout.error = getString(errorMessage)
        emailLayout.isErrorEnabled = true
    }

    override fun displayPasswordError(errorMessage: Int, passwordsMismatch: Boolean) {
        passwordLayout.error = getString(errorMessage)
        passwordLayout.isErrorEnabled = true
        if(passwordsMismatch) {
            confirmPasswordLayout.error = getString(errorMessage)
            confirmPasswordLayout.isErrorEnabled = true
        }
    }

    private fun disableLayouts() {
        this.emailLayout.isClickable = false
        this.emailLayout.isFocusable = false
        this.passwordLayout.isClickable = false
        this.passwordLayout.isFocusable = false
        this.confirmPasswordLayout.isClickable = false
        this.confirmPasswordLayout.isFocusable = false
        this.signupButton.isClickable = false
    }

    private fun enableLayouts() {
        this.emailLayout.isClickable = true
        this.emailLayout.isFocusable = true
        this.passwordLayout.isClickable = true
        this.passwordLayout.isFocusable = true
        this.confirmPasswordLayout.isClickable = true
        this.confirmPasswordLayout.isFocusable = true
        this.signupButton.isClickable = true
    }

    override fun displayProgressBar() {
        this.progressBar.visibility = View.VISIBLE
        disableLayouts()
    }

    override fun hideProgressBar() {
        this.progressBar.visibility = View.GONE
        enableLayouts()
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType) {
        super.displaySnackbarMessage(message, length, messageType)
    }

    override fun launchHomeActivity() {
        startActivity(Intent(this, HomeActivity::class.java))
    }
}