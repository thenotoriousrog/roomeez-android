package com.appme.roomeez.authentication

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class ForgotPasswordActivity : BaseActivity(), ForgotPasswordActivityUpdater {

    private val presenter = ForgotPasswordActivityPresenter(this)
    private lateinit var container: ConstraintLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var recoveryEmailLayout: TextInputLayout
    private lateinit var sendRecoveryEmailButton: MaterialButton

    override fun getLayoutResId(): Int {
        return R.layout.forgot_password_activity
    }

    override fun getSnackbarContainerView(): View {
        return container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.forgot_your_password)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.container = findViewById(R.id.forgot_password_activity_container)
        this.progressBar = findViewById(R.id.progress_bar)
        this.recoveryEmailLayout = findViewById(R.id.recovery_email_layout)
        this.sendRecoveryEmailButton = findViewById(R.id.send_recovery_email_button)
        sendRecoveryEmailButton.setOnClickListener {
            recoveryEmailLayout.isErrorEnabled = false
            presenter.attemptPasswordRecovery(recoveryEmailLayout.editText?.text.toString())
        }
    }

    override fun displayEmailError(errorMessage: Int) {
        recoveryEmailLayout.error = getString(errorMessage)
        recoveryEmailLayout.isErrorEnabled = true
    }

    override fun displayProgressBar() {
        this.progressBar.visibility = View.VISIBLE
        this.sendRecoveryEmailButton.isClickable = false
    }

    override fun hideProgressBar() {
        this.progressBar.visibility = View.GONE
        this.sendRecoveryEmailButton.isClickable = true
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType) {
        super.displaySnackbarMessage(message, length, messageType) // call to super method
    }

    override fun displayToast(message: Int, length: Int) {
        super.displayToastMessage(message, length)
    }

    override fun killActivity() {
        finish()
    }
}