package com.appme.roomeez

import com.appme.roomeez.R

/**
 * Used to determine the type of message we want to display which can vary in color depending on the type of message we're displaying to the user
 */
enum class SnackbarMessageType(val color: Int) {
    PRIORITY(R.color.colorComplimentary), // a message that the user is likely going to want to see
    SECONDARY(R.color.colorAccent), // a message that the user likely won't care about but would be useful to see
    ERROR(R.color.red) // used to give extra information if something they did is unable to continue for any reason
}