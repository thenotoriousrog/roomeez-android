package com.appme.roomeez.backend

import java.lang.StringBuilder
import java.util.*

/**
 * This class represents the data needed for a date object to be presented in a house hold for a user
 * It is not to be confused with the Date classes in Java/Kotlin
 */
class Date() {

    private val monthMap = mapOf<Int, String>( // hold map of months with 0 index based items
        0 to "January", 1 to "February",
        2 to "March", 3 to "April",
        4 to "May", 5 to "June",
        6 to "July", 7 to "August",
        8 to "September", 9 to "October",
        10 to "November", 11 to "December")

    private val weekMap = mapOf<Int, String>( // hold map of days in the week!
        1 to "Sunday", 2 to "Monday",
        3 to "Tuesday", 4 to "Wednesday",
        5 to "Thursday", 6 to "Friday", 7 to "Saturday")

    /**
     * Returns the current date with day in the week, day and year i.e. Friday, August 27th, 2019
     */
    fun getCurrentDate(): String {
        val calendar = Calendar.getInstance()
        val month = calendar.get(Calendar.MONTH)
        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        val year = calendar.get(Calendar.YEAR)

        val monthName = monthMap[month]
        val dayName = weekMap[dayOfWeek]

        return StringBuilder()
            .append(dayName).append(", ") // i.e. Friday,
            .append(monthName).append(" ") // i.e. August
            .append(dayOfMonth).append("th, ") // i.e. 27th,
            .append(year) // i.e 2019
            .toString() // i.e. Friday, August 27th, 2019
    }

    fun getReminderType(): ReminderType {
        return ReminderType.DATE
    }
}