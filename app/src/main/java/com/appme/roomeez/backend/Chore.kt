package com.appme.roomeez.backend

/**
 * This class represents the data that is present in a Chore for a house.
 * @param dateCreated - The date of when this chore was created.
 * @param choreTitle - the title of this particular chore
 * @param choreCreator - the creator of this chore.
 * @param choreDescription - an optional description of this chore or instructions on how to complete this chore completely
 * @param profileImageUrl - an optional url for a custom image that a user can chose to upload
 * @param choreAssignee - the person the chore is assigned to
 * @param dateRange - the range of the date for this particular chore
 * @param choreProgressPercentage - how close the roomeee to completing this chore cycle
 * @param nextAssignee - an optional name for the roomee that is to be assigned next for this chore
 * @param choreCycle - the cycle for this particular chore
 * @param totalDaysInCycle - in a given date range, these are the amount of days in the cycle
 * @param totalDaysCompletedInCycle - the total amount of day completed for this chore's cycle
 */
data class Chore(val dateCreated: String,
                 val choreCreator: String,
                 val choreTitle: String,
                 val choreDescription: String?,
                 val profileImageUrl: String?,
                 val choreAssignee: String,
                 val dateRange: String,
                 val choreProgressPercentage: Float,
                 val nextAssignee: String?,
                 val choreCycle: ChoreCycle,
                 val totalDaysInCycle: Int,
                 val totalDaysCompletedInCycle: Int) {

    fun getReminderType(): ReminderType {
        return ReminderType.CHORE
    }
}