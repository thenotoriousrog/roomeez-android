package com.appme.roomeez.backend

import java.util.Date

/**
 * This class represents a bill that is seen in a household on the Home menu
 * @param dateCreated - the date the bill was created.
 * @param author - the creator of this bill
 * @param billTitle - the title of this particular bill
 * @param amountDue - the amount of money due for this bill
 * @param dueDate - the date that this bill is due
 * @param dueDateProgress - the current progress that's near the progress
 * @param isRecurring - tells whether or not this bill is recurring or not
 */
data class Bill(val dateCreated: String,
                val author: String,
                val billTitle: String,
                val amountDue: Int,
                val dueDate: String,
                val dueDateProgress: Float,
                val isRecurring: Boolean) {

    fun getReminderType(): ReminderType {
        return ReminderType.BILL
    }
}