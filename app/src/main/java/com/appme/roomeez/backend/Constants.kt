package com.appme.roomeez.backend

/**
 * A group of general constants to be used throughout the application whereever needed.
 */
class Constants {
    companion object {
        // **Note: Add constants here and remove this line upon the first constant that is added.
    }
}