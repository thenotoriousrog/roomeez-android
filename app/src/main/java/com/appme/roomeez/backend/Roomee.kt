package com.appme.roomeez.backend

/**
 * This class represents an Roomee. This can be the current user as well as other roomeez.
 * @param name - the real name of this roomee
 * @param displayName - the display name used by this roomee
 * @param email - the email used by this roomee to sign up
 * @param age - how old the roomee is
 * @param gender - the gender specified by this user
 * @param roomeezScore - the score of this particular roomee
 */
data class Roomee(val name: String,
                  val displayName: String?,
                  val email: String,
                  val age: Int,
                  val gender: String?,
                  val roomeezScore: Float)