package com.appme.roomeez.backend

/**
 * This class represents a general note for a household.
 * @param dateCreated - the date that this item was created
 * @param author - the creator of this note
 * @param note - the itself to be displayed to the house
 * @param profilePictureUrl - an optional url for retrieving that user's profile picture from Firebase.
 */
data class Note(val dateCreated: String,
           val author: String,
           val note: String,
           val profilePictureUrl: String?) {

    fun getReminderType(): ReminderType {
        return ReminderType.NOTE
    }
}