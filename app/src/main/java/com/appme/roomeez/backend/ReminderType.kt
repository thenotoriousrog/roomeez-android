package com.appme.roomeez.backend

import com.appme.roomeez.R

/**
 * This class represents the different types of reminders that can be presented on the user's screen.
 * The reminder determines the UI to be used when generating it on the home screen
 * @param layoutRes - the layout resource associated for this type of reminder.
 * @param viewTypeId - this gives an integer based definition on what this view is. Used when generating UI in recycler view
 */
enum class ReminderType(val layoutRes: Int, val viewTypeId: Int) {
    CHORE(R.layout.chore_reminder_layout, 1000),
    BILL(R.layout.bill_reminder_layout, 2000),
    DATE(R.layout.date_reminder_layout, 3000),
    NOTE(R.layout.note_reminder_layout, 4000)
}