package com.appme.roomeez.backend.persistance.listeners

/**
 * General listener that will report back various events when Firebase finishes the task that we assign to it.
 * In most cases, this is the only listener needed to get events back from firebase and continue working on the application.
 */
interface FirebaseListener {

    /**
     * Reports that an action was cancelled and thus firebase was unable to complete the task assigned to it.
     */
    fun onCancel()

    /**
     * Reports an error back to the calling class with a message from the string res folder.
     * Failure indicates that something is wrong, but could be something that is external such as a bad internet connection
     */
    fun onFail()

    /**
     * The database failed in someway. Reporting the error back to the calling class.
     */
    fun onError()

    /**
     * Reports back to the calling class that the requested action was successful
     */
    fun onSuccess()
}