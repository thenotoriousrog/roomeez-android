package com.appme.roomeez.backend.persistance

import android.util.Log
import com.appme.roomeez.R
import com.appme.roomeez.backend.persistance.listeners.FirebaseListener
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase


/**
 * This class is used to help with various backend behaviors that need to be used when we interact with Firebase.
 * This class can be used anywhere in the project to allow modification in the database through the app's entire lifecycle.
 */
class FirebaseHelper : FirebaseAuth.AuthStateListener {

    private val TAG = "FirebaseHelper"
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()

    companion object {
        val instance = FirebaseHelper() // Our singleton. This will only get created once even if called multiple times.
    }

    private fun getUserId(): String? {
        return auth.currentUser?.uid
    }

    fun getUserName(): String? {
        return auth.currentUser?.displayName
    }

    fun isLoggedIn(): Boolean {
        return getUserId() != null
    }

    /**
     * Creates the user's account and signs them in.
     * @param email - the user's verified email
     * @param password - the user's verified password
     * @param listener - optional listener to passback information if the caller desires to do so
     */
    fun signupUser(email: String, password: String, listener: FirebaseListener?) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if(!task.isSuccessful) {
                listener?.onFail()
                Log.e(TAG, "Unable to create user account! ${task.exception?.message}")
            } else {
                val userId = getUserId()
                if(userId.isNullOrBlank()) { // this shouldn't be null here something really odd is happening.
                    listener?.onError()
                    Log.wtf(TAG, "Unexpected error in database after creating user account! ${task.exception?.message}")
                } else {
                    listener?.onSuccess()
                }
            }
        }
    }

    /**
     * Logs in a user
     * @param email - the user's verified email
     * @param password - the user's entered password
     * @param listener - optional listener to respond to actions if the caller desires.
     */
    fun loginUser(email: String, password: String, listener: FirebaseListener?) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if(!task.isSuccessful) {
                listener?.onFail()
            } else {
                val userId = getUserId()
                if(userId == null) {
                    listener?.onError()
                    Log.wtf(TAG, "User logged in but without a userID! Unexpected error! ${task.exception?.message}")
                } else {
                    listener?.onSuccess()
                }
            }

            if(task.isCanceled) {
                listener?.onCancel()
            }
        }
    }

    /**
     * Logs the user in with a google account after the credentials have been passed through. This requires
     * @param authCredential - the auth credential retrieved from Google's auth process.
     * @param listener - optinal listener to fireback actions to the caller
     */
    fun loginUserWithGoogle(authCredential: AuthCredential, listener: FirebaseListener?) {
        auth.signInWithCredential(authCredential).addOnCompleteListener { task ->
            when {
                task.isCanceled -> listener?.onCancel()
                task.isSuccessful -> listener?.onSuccess()
                else -> { // task failed, need to alert the user
                    listener?.onFail()
                    Log.w(TAG, "Could not log user in with the provided authCredential!")
                }

            }
        }
    }

    /**
     * Sends an email for a user to be able to reset their password if they happen to forget their password
     * @param email - the user's verified email
     * @param listener - optional listener to get callbacks
     */
    fun sendPasswordResetEmail(email: String, listener: FirebaseListener?) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if(task.isCanceled) {
                listener?.onCancel()
            }

            if(task.isSuccessful) {
                listener?.onSuccess()
            } else {
                listener?.onFail()
                Log.e(TAG, "Firebase was unable to send recovery email!")
            }
        }
    }

    /**
     * Signs the user out
     */
    fun signOut() {
        this.auth.signOut()
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        this.auth = auth
    }
}