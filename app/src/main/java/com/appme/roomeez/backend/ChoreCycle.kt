package com.appme.roomeez.backend

/**
 * Describes the length of a cycle for a particular chore. Actual days are implemented in the chore itself
 */
enum class ChoreCycle() {
    DAILY,
    WEEKLY,
    SEMI_MONTHLY,
    MONTHLY,
    CUSTOM
}