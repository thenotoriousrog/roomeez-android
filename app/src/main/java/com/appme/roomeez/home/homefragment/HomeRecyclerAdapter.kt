package com.appme.roomeez.home.homefragment

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appme.roomeez.R
import com.appme.roomeez.backend.ReminderType
import com.appme.roomeez.backend.inflate

/**
 * Custom adapter to generate various differing views and layout based on the type of object coming into the recycler view!
 */
class HomeRecyclerAdapter(private val presenter: HomeRecyclerViewPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "HomeRecyclerAdapter"

    /**
     * Tracks the reminderType from item type
     * @param viewType - the item type for this recycler view
     */
    private fun getReminderType(viewType: Int): ReminderType? {
        return when(viewType) {
            ReminderType.CHORE.viewTypeId -> ReminderType.CHORE
            ReminderType.DATE.viewTypeId -> ReminderType.DATE
            ReminderType.BILL.viewTypeId -> ReminderType.BILL
            ReminderType.NOTE.viewTypeId -> ReminderType.NOTE
            else -> null
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.i(TAG, "onCreateViewHolder called!")
        return when(viewType) {
            ReminderType.CHORE.viewTypeId -> ChoreViewHolder(parent.inflate(ReminderType.CHORE.layoutRes))
            ReminderType.DATE.viewTypeId -> DateViewHolder(parent.inflate(ReminderType.DATE.layoutRes))
            ReminderType.BILL.viewTypeId -> BillViewHolder(parent.inflate(ReminderType.BILL.layoutRes))
            ReminderType.NOTE.viewTypeId -> NoteViewHolder(parent.inflate(ReminderType.NOTE.layoutRes))
            else -> throw IllegalArgumentException("Unable to create view holder! Unknown ReminderType! ViewType == $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        Log.i(TAG, "onBindViewHolder called!")

        when(val reminderType = getReminderType(holder.itemViewType)) {
            ReminderType.CHORE -> {
                val choreViewHolder = holder as ChoreViewHolder
                presenter.onBindChoreItemAtPosition(choreViewHolder, position)
            }
            ReminderType.DATE ->  {
                val dateViewHolder = holder as DateViewHolder
                presenter.onBindDateItemAtPosition(dateViewHolder, position)
            }
            ReminderType.NOTE -> {
                val noteViewHolder = holder as NoteViewHolder
                presenter.onBindNoteItemAtPosition(noteViewHolder, position)
            }
            ReminderType.BILL -> {
                val billViewHolder = holder as BillViewHolder
                presenter.onBindBillItemAtPosition(holder, position)
            }
            else -> Log.w(TAG, "Unable to bind view older! ReminderType == ${reminderType?.name}")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return presenter.getViewTypeFromPosition(position)
    }

    override fun getItemCount(): Int {
        return presenter.getReminderCount()
    }
}