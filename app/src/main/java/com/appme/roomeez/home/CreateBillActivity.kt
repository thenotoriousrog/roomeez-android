package com.appme.roomeez.home

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ScrollView
import android.widget.Toast
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.checkbox.MaterialCheckBox
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class CreateBillActivity : BaseActivity(), CreateBillActivityUpdater {

    private val presenter = CreateBillActivityPresenter(this)
    private lateinit var container: ScrollView
    private lateinit var billTitleLayout: TextInputLayout
    private lateinit var billDescriptionLayout: TextInputLayout
    private lateinit var billAmountDueLayout: TextInputLayout
    private lateinit var reoccuresMonthlyCheckBox: MaterialCheckBox
    private lateinit var createBillButton: MaterialButton

    override fun getLayoutResId(): Int {
        return R.layout.create_bill_activity
    }

    override fun getSnackbarContainerView(): View {
        return container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.create_bill_activity_title)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.container = findViewById(R.id.create_bill_container)
        this.billTitleLayout = findViewById(R.id.bill_title_layout)
        this.billDescriptionLayout = findViewById(R.id.bill_description_layout)
        this.billAmountDueLayout = findViewById(R.id.bill_amount_layout)
        this.reoccuresMonthlyCheckBox = findViewById(R.id.reoccures_monthly_checkbox)

        this.createBillButton = findViewById(R.id.create_bill_button)
        this.createBillButton.setOnClickListener {
            billTitleLayout.isErrorEnabled = false
            billAmountDueLayout.isErrorEnabled = false

            val billTitle = billTitleLayout.editText?.text?.toString()
            val billDescription = billDescriptionLayout.editText?.text?.toString()
            val billAmountDue = billAmountDueLayout.editText?.text?.toString()
            val reoccuresMonthly = reoccuresMonthlyCheckBox.isSelected

            presenter.createBillButtonClicked(billTitle, billDescription, billAmountDue, reoccuresMonthly)
        }
    }

    override fun displayAmountDueError(error: Int) {
        billAmountDueLayout.error = getString(error)
        billAmountDueLayout.isErrorEnabled = true
    }

    override fun displayTitleError(error: Int) {
        billTitleLayout.error = getString(error)
        billTitleLayout.isErrorEnabled = true
    }

    override fun killActivity() {
        displayToastMessage(R.string.item_added_later_toast, Toast.LENGTH_SHORT) // fixme: this can be removed once database integration is completed!
        finish()
    }
}