package com.appme.roomeez.home

import com.appme.roomeez.R

class CreateBillActivityPresenter(private val updater: CreateBillActivityUpdater) {


    fun createBillButtonClicked(title: String?, billDescription: String?,
                                amountDue: String?, reoccuresMonthly: Boolean) {

        if(title.isNullOrBlank()) {
            updater.displayTitleError(R.string.title_empty)
            return
        }

        if(amountDue.isNullOrBlank()) {
            updater.displayAmountDueError(R.string.amount_due_empty)
            return
        }

        // TODO: need to create the bill and add it to the database for this household. Alert all roomeez
        updater.killActivity()
    }
}