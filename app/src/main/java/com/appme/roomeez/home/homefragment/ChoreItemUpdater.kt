package com.appme.roomeez.home.homefragment

/**
 * Used to properly update a Chore item
 */
interface ChoreItemUpdater {

    /**
     * Sets the name of this chore
     * @param title - what this chore is actually called
     */
    fun setTitle(title: String)

    /**
     * Sets the profile image using glide. If null, uses default image
     * @param url - the url to the user's profile image. If one exists.
     */
    fun setProfileImage(url: String?)

    /**
     * Sets the name of the chore that it is assigned too
     * @param assignee - the person responsible for this chore
     */
    fun setChoreAssignee(assignee: String)

    /**
     * Sets the date range for this particular chore
     * @param dateRange - the range from start to finish for this chore
     */
    fun setDateRange(dateRange: String)

    /**
     * Sets the progress bar on the chore giving a visualization of how close the user is to completing this chore
     * @param percentage - the percentage. 100% being fully completed and ready to move on to next assignee
     */
    fun setChoreProgress(percentage: Float)

    /**
     * Sets the name of the next assignee for this chore!
     * @param nextAssignee - the next person to be doing this chore
     */
    fun setNextAssignee(nextAssignee: String)

}