package com.appme.roomeez.home

import com.appme.roomeez.R

class CreateNoteActivityPresenter(private val updater: CreateNoteActivityUpdater) {

    fun createNoteButtonClicked(noteContent: String?) {
        if(noteContent.isNullOrBlank()) {
            updater.displayNoteError(R.string.note_empty_error)
            return
        }

        // TODO: need to add note to database and add it to the household
        updater.killActivity()
    }

}