package com.appme.roomeez.home

interface CreateBillActivityUpdater {

    /**
     * Displays an error on the title text input view
     * @param error - the error in a string resource
     */
    fun displayTitleError(error: Int)

    /**
     * Displays an error around the amount due text input.
     * @param error - the error to display
     */
    fun displayAmountDueError(error: Int)

    fun killActivity()
}