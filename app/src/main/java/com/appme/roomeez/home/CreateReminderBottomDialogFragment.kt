package com.appme.roomeez.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.appme.roomeez.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.textview.MaterialTextView
import kotlinx.android.synthetic.*
import java.lang.IllegalArgumentException

/**
 * A simple listener to report back when a reminder is clicked
 */
interface CreateReminderBottomDialogListener {
    fun onCreateChoreSelected()
    fun onCreateBillSelected()
    fun onCreateNoteSelected()
}

/**
 * A simple custom BottomSheetDialogFragment to help the user create a new reminder.
 */
class CreateReminderBottomDialogFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private val TAG = "CreateRemindrBottomDiag"
    private lateinit var createChoreTextView: MaterialTextView
    private lateinit var createBillTextView: MaterialTextView
    private lateinit var createNoteTextView: MaterialTextView
    private var listener: CreateReminderBottomDialogListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.create_reminder_dialog_fragment_layout, container, false)

        this.createChoreTextView = view.findViewById(R.id.create_chore_text)
        this.createChoreTextView.setOnClickListener(this)

        this.createBillTextView = view.findViewById(R.id.create_bill_text)
        this.createBillTextView.setOnClickListener(this)

        this.createNoteTextView = view.findViewById(R.id.create_note_text)
        this.createNoteTextView.setOnClickListener(this)

        return view
    }

    fun setCreateReminderClickListener(listener: CreateReminderBottomDialogListener) {
        this.listener = listener
    }

    override fun onClick(viewClicked: View?) {
        when(viewClicked?.id) {
            this.createChoreTextView.id -> {
                listener?.onCreateChoreSelected()
                this.dismiss()
            }

            this.createBillTextView.id -> {
                listener?.onCreateBillSelected()
                this.dismiss()
            }

            this.createNoteTextView.id -> {
                listener?.onCreateNoteSelected()
                this.dismiss()
            }

            null -> {
                Log.e(TAG, "View clicked was null! Closing dialog!")
                this.dismiss()
            }
            else -> {
                Log.w(TAG, "Unable to respond to click for view:${viewClicked.id}. Ignoring and closing dialog")
                this.dismiss()
            }
        }
    }

}