package com.appme.roomeez.home

interface CreateNoteActivityUpdater {

    /**
     * Displays an error if content of the note is empty
     * @param error - the error to be displayed to the user
     */
    fun displayNoteError(error: Int)

    fun killActivity()
}