package com.appme.roomeez.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.ActionMenuView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.R
import com.appme.roomeez.home.homefragment.HomeFragment
import com.appme.roomeez.home.homefragment.HomeFragmentCallback
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.google.android.material.floatingactionbutton.FloatingActionButton


/**
 * The main activity where the user is going to be spending most of their time
 */
class HomeActivity : BaseActivity(), HomeActivityUpdater, HomeFragmentCallback, CreateReminderBottomDialogListener {

    private val TAG = "HomeActivity"
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var bottomAppBar: BottomAppBar
    private lateinit var messagesFAB: FloatingActionButton
    private lateinit var createReminderButton: ExtendedFloatingActionButton
    private val createReminderFragment = CreateReminderBottomDialogFragment()

    override fun getLayoutResId(): Int {
        return R.layout.home_activity
    }

    override fun getSnackbarContainerView(): View {
        return this.coordinatorLayout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.coordinatorLayout = findViewById(R.id.home_activity_container)
        this.createReminderFragment.setCreateReminderClickListener(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val homeFragment = HomeFragment()
        homeFragment.setCallback(this)
        fragmentTransaction.add(R.id.fragment_container, homeFragment)
        fragmentTransaction.commit()

        this.bottomAppBar = findViewById(R.id.bottom_app_bar)

        this.bottomAppBar.setNavigationOnClickListener {
            // TODO: implement this!
        }

        this.messagesFAB = findViewById(R.id.messages_fab)
        this.messagesFAB.setOnClickListener {
            // TODO: Implement this, need to create the ui to create a type of reminder! The result will get pushed to firebase for processing!
        }

        this.createReminderButton = findViewById(R.id.create_reminder_button)
        this.createReminderButton.setOnClickListener {
            createReminderFragment.show(supportFragmentManager, "create_reminder_dialog_fragment")
        }
    }

    override fun shrinkCreateReminderButton() {
        this.createReminderButton.shrink(true)
    }

    override fun extendCreateReminderButton() {
        this.createReminderButton.extend(true)
    }

    override fun onCreateChoreSelected() {
        startActivity(Intent(this, CreateChoreActivity::class.java))
    }

    override fun onCreateBillSelected() {
        startActivity(Intent(this, CreateBillActivity::class.java))
    }

    override fun onCreateNoteSelected() {
        startActivity(Intent(this, CreateNoteActivity::class.java))
    }
}