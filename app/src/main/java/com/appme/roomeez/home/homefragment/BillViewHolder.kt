package com.appme.roomeez.home.homefragment

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar
import com.appme.roomeez.R

/**
 * Custom ViewHolder to properly generate the Bill view
 */
class BillViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), BillItemUpdater {

    private val billTitleTextView: TextView = itemView.findViewById(R.id.bill_title)
    private val amountDueTextView: TextView = itemView.findViewById(R.id.amount_due_value)
    private val dueDateTextView: TextView = itemView.findViewById(R.id.due_date)
    private val dueDateProgress: RoundCornerProgressBar = itemView.findViewById(R.id.due_date_progress)

    override fun setTitle(title: String) {
        this.billTitleTextView.text = title
    }

    override fun setAmountDue(amountDue: String) {
        this.amountDueTextView.text = amountDue
    }

    override fun setDueDate(dueDate: String) {
        this.dueDateTextView.text = dueDate
    }

    override fun setBillProgress(percentage: Float) {
        this.dueDateProgress.progress = percentage // fixme: this most likely won't work on it's own. We will likely want to set the progress!
    }
}