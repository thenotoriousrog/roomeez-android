package com.appme.roomeez.home

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSpinner
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.appme.roomeez.backend.Roomee
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.textfield.TextInputLayout

/**
 * Parent activity for this activity is HomeActivity
 */
class CreateChoreActivity() : BaseActivity(), CreateChoreActivityUpdater {

    private val TAG = "CreateChoreActivity"
    private val presenter = CreateChoreActivityPresenter(this)
    private lateinit var createChoreContainer: ScrollView
    private lateinit var progressBar: ProgressBar
    private lateinit var roomeezChipGroup: ChipGroup
    private lateinit var choreTitleInput: TextInputLayout
    private lateinit var choreDescInput: TextInputLayout
    private lateinit var choreCycleSpinner: AppCompatSpinner
    private lateinit var createChoreButton: MaterialButton

    override fun getLayoutResId(): Int {
        return R.layout.create_chore_activity
    }

    override fun getSnackbarContainerView(): View {
        return createChoreContainer
    }

    /**
     * Simply grabs the UI name from the roomee. It is NOT an identifier for a valid roomee object.
     * The CreateChoreActivityPresenter will validate the selected name with the proper roomee
     */
    private fun getSelectedRoomeeName(): String? {
        val selectedChipId: Int = this.roomeezChipGroup.checkedChipId

        if(selectedChipId == -1) {
            return null
        }

        val selectedChip: Chip = roomeezChipGroup.findViewById(selectedChipId)
        return selectedChip.text.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.create_chore_activity_title)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.progressBar = findViewById(R.id.chore_progress_bar)
        this.createChoreContainer = findViewById(R.id.create_chore_container)
        this.roomeezChipGroup = findViewById(R.id.roomeez_chip_group) // TODO: This needs to be populated from a list of roomeez in the backend! Very important! See the overridden method!
        this.choreTitleInput = findViewById(R.id.chore_title_layout)
        this.choreDescInput = findViewById(R.id.chore_desc_instruct_layout)
        this.choreCycleSpinner = findViewById(R.id.chore_cycle_spinner)
        this.createChoreButton = findViewById(R.id.create_chore_button)

        this.createChoreButton.setOnClickListener {
            this.choreTitleInput.isErrorEnabled = false // disable the error display in case the user modified the text field.
            val selectedRoomee = getSelectedRoomeeName()
            val choreTitle = choreTitleInput.editText?.text?.toString()
            val choreDescription = choreDescInput.editText?.text?.toString()
            val choreCycleSelected = choreCycleSpinner.selectedItem.toString()

            presenter.createChoreButtonClicked(choreTitle, choreDescription, choreCycleSelected, selectedRoomee)
        }
    }

    override fun showProgressBar() {
        this.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        this.progressBar.visibility = View.GONE
    }

    override fun updateRoomeezChipGroup(roomeezList: List<Roomee>) {
        // TODO: This needs to be implemented in order to get the real list of roomeez for a particular household
    }

    override fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType) {
        super.displaySnackbarMessage(message, length, messageType)
    }

    override fun displayTitleError(error: Int) {
        this.choreTitleInput.error = getString(error)
        this.choreTitleInput.isErrorEnabled = true
    }

    override fun killActivity() {
        displayToastMessage(R.string.item_added_later_toast, Toast.LENGTH_SHORT) // fixme: this can be removed once database integration is completed!
        finish()
    }
}