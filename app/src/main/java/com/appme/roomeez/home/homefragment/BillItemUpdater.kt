package com.appme.roomeez.home.homefragment

/**
 * Used to properly update an item for a Bill object
 */
interface BillItemUpdater {

    /**
     * @param title - the title of this Bill
     */
    fun setTitle(title: String)

    /**
     * The proper amount due for this item
     * @param amountDue - dollar amount due for this item
     */
    fun setAmountDue(amountDue: String)

    /**
     * @param dueDate - the date that this Bill is due
     */
    fun setDueDate(dueDate: String)

    /**
     * Sets the progressBar progress
     * @param percentage - the percentage of time left before this bill is due. 100% meaning due at the day of viewing this item!
     */
    fun setBillProgress(percentage: Float)
}