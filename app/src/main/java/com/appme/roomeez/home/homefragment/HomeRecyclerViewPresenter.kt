package com.appme.roomeez.home.homefragment

import android.util.Log
import com.appme.roomeez.backend.*
import java.lang.IllegalArgumentException

/**
 * Used to separate logic away from the MainActivity
 * @param reminders - a list of generic objects which can be of various reminder types i.e Chores, Bills, and/or Notes
 * List can be updated to account for updating the home for any new items
 */
class HomeRecyclerViewPresenter(private var reminders: List<Any>) {

    private val TAG = "HomeRecyclerPresenter"

    fun updateReminders(reminders: List<Any>) {
        this.reminders = reminders
    }

    fun onBindChoreItemAtPosition(updater: ChoreItemUpdater, position: Int) {
        val chore = reminders[position] as Chore
        updater.setTitle(chore.choreTitle)
        updater.setChoreAssignee(chore.choreAssignee)
        updater.setNextAssignee(chore.nextAssignee ?: chore.choreAssignee) // if no assignee, default to using the current chore assignee. Likely because only one person is on this chore
        updater.setDateRange(chore.dateRange)
        updater.setChoreProgress(chore.choreProgressPercentage)
        updater.setProfileImage(chore.profileImageUrl)
    }

    fun onBindBillItemAtPosition(updater: BillItemUpdater, position: Int) {
        val bill = reminders[position] as Bill
        updater.setTitle(bill.billTitle)
        updater.setAmountDue("$" + bill.amountDue.toString())
        updater.setBillProgress(bill.dueDateProgress)
        updater.setDueDate(bill.dueDate)
    }

    fun onBindNoteItemAtPosition(updater: NoteItemUpdater, position: Int) {
        val note = reminders[position] as Note
        updater.setNoteText(note.note)
        updater.setCreatorProfileImage(note.profilePictureUrl)
        updater.setAuthorName(note.author)
    }

    fun onBindDateItemAtPosition(updater: DateItemUpdater, position: Int) {
        val date = reminders[position] as Date
        updater.setCurrentDate(date.getCurrentDate())
    }

    fun getViewTypeFromPosition(position: Int): Int {

        return when (reminders[position]) {
            is Chore -> ReminderType.CHORE.viewTypeId
            is Note -> ReminderType.NOTE.viewTypeId
            is Bill -> ReminderType.BILL.viewTypeId
            is Date -> ReminderType.DATE.viewTypeId
            else -> throw IllegalArgumentException("Unknown ReminderType received for HomeRecyclerView. Unable to properly generate view!")
        }
    }

    fun getReminderCount(): Int {
        return this.reminders.size
    }
}