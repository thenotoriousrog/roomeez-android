package com.appme.roomeez.home

import android.os.Bundle
import android.view.View
import android.widget.ScrollView
import android.widget.Toast
import com.appme.roomeez.BaseActivity
import com.appme.roomeez.R
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout

class CreateNoteActivity : BaseActivity(), CreateNoteActivityUpdater {

    private val presenter = CreateNoteActivityPresenter(this)
    private lateinit var container: ScrollView
    private lateinit var noteContentLayout: TextInputLayout
    private lateinit var createNoteButton: MaterialButton

    override fun getLayoutResId(): Int {
        return R.layout.create_note_activity
    }

    override fun getSnackbarContainerView(): View {
        return container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.title = getString(R.string.create_note_activity_title)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.back_arrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        this.container = findViewById(R.id.create_note_container)
        this.noteContentLayout = findViewById(R.id.note_content_layout)

        this.createNoteButton = findViewById(R.id.create_note_button)
        this.createNoteButton.setOnClickListener {
            noteContentLayout.isErrorEnabled = false

            val noteContent = noteContentLayout.editText?.text?.toString()
            presenter.createNoteButtonClicked(noteContent)
        }
    }

    override fun displayNoteError(error: Int) {
        this.noteContentLayout.error = getString(error)
        this.noteContentLayout.isErrorEnabled = true
    }

    override fun killActivity() {
        displayToastMessage(R.string.item_added_later_toast, Toast.LENGTH_SHORT) // fixme: this can be removed once database integration is completed!
        finish()
    }
}