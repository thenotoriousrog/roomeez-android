package com.appme.roomeez.home.homefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appme.roomeez.R
import com.appme.roomeez.backend.*
import com.appme.roomeez.home.HomeActivity
import kotlin.collections.ArrayList

/**
 * This interface makes interacting with the HomeActivity much easier and adds an extra layer of abstraction.
 */
interface HomeFragmentCallback {
    fun extendCreateReminderButton()
    fun shrinkCreateReminderButton()
}

/**
 * This fragment displays the home screen that user's are going to spend most of their time
 */
class HomeFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var recyclerViewAdapter: HomeRecyclerAdapter
    private lateinit var recyclerPresenter: HomeRecyclerViewPresenter
    private var callback: HomeFragmentCallback? = null

    private fun generateFakeReminders(): List<Any> { // fixme: This must not be used going forward. This is fake data and we must use firebase to grab our data! Very important!
        val list = ArrayList<Any>()

        val date = Date()

        val chore1 = Chore("08/30/2019", "Roger",
            "Dishes", null, "Roger",
            "Steven", "August 30th - September 10th", 55.0f,
            "Josh", ChoreCycle.WEEKLY, 7, 4)

        val chore2 = Chore("08/30/2019", "Steven",
            "Trash", null, "Steven",
            "Steven", "August 30th - September 10th", 55.0f,
            "Josh", ChoreCycle.WEEKLY, 7, 4)

        val chore3 = Chore("08/30/2019", "Josh",
            "Vaccum Living Room", null, "Josh",
            "Steven", "August 30th - September 10th", 55.0f,
            "Josh", ChoreCycle.WEEKLY, 7, 4)

        val bill1 = Bill("08/30/2019", "Russo", "Rent",
            1500, "September 1st, 2019", 90.0f, true)

        val bill2 = Bill("08/30/2019", "Aaron", "Electricity",
            120, "September 1st, 2019", 90.0f, true)

        val bill3 = Bill("08/30/2019", "Roger", "Water",
            60, "September 1st, 2019", 90.0f, true)

        val note1 = Note("08/30/2019", "Josh", "Steven has a gargantuan gut", null)
        val note2 = Note("08/30/2019", "Steven", "Don't EVER take my food in the fridge again", null)
        val note3 = Note("08/30/2019", "Aaron", "I'll be giving my dog a bath this Sunday", null)

        list.add(date)
        list.add(chore1)
        list.add(chore2)
        list.add(chore3)
        list.add(bill1)
        list.add(bill2)
        list.add(bill3)
        list.add(note1)
        list.add(note2)
        list.add(note3)

        return list
    }

    fun setCallback(callback: HomeFragmentCallback) {
        this.callback = callback
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        this.recyclerPresenter = HomeRecyclerViewPresenter(generateFakeReminders())
        this.recyclerViewAdapter = HomeRecyclerAdapter(recyclerPresenter)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val homeView = inflater.inflate(R.layout.home_fragment_layout, container, false)
        this.recyclerView = homeView.findViewById(R.id.home_reminders_recycler_view)
        this.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                if(dy > 0) { // scrolling up
                    callback?.shrinkCreateReminderButton()
                } else if(dy < 0) { // scrolling up
                    callback?.extendCreateReminderButton()
                }
            }
        })

        this.recyclerView.adapter = recyclerViewAdapter
        this.recyclerView.layoutManager = LinearLayoutManager(context)
        this.recyclerViewAdapter.notifyDataSetChanged()

        return homeView
    }

}