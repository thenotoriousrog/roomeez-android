package com.appme.roomeez.home.homefragment

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar
import com.appme.roomeez.R
import com.bumptech.glide.Glide
import org.w3c.dom.Text

/**
 * This view holder is responsible for creating the view for the
 */
class ChoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), ChoreItemUpdater {

    private val titleTextView: TextView = itemView.findViewById(R.id.chore_title)
    private val profileImageView: ImageView = itemView.findViewById(R.id.profile_image)
    private val choreAssigneeTextView: TextView = itemView.findViewById(R.id.chore_asignee)
    private val dateAssignedRangeTextView: TextView = itemView.findViewById(R.id.date_asigned_range)
    private val roundProgressBar: RoundCornerProgressBar = itemView.findViewById(R.id.due_date_progress)
    private val upnextTextView: TextView = itemView.findViewById(R.id.up_next_roomee)

    override fun setTitle(title: String) {
        this.titleTextView.text = title
    }

    override fun setProfileImage(url: String?) {
        Glide.with(itemView.context)
            .load(url)
            .error(R.drawable.ic_account_circle)
            .placeholder(R.drawable.ic_account_circle)
            .into(profileImageView)
    }

    override fun setChoreAssignee(assignee: String) {
        this.choreAssigneeTextView.text = assignee
    }

    override fun setDateRange(dateRange: String) {
        this.dateAssignedRangeTextView.text = dateRange
    }

    override fun setChoreProgress(percentage: Float) { // fixme: this may not actually work for properly holding the percentage in this item. It is crucial to get this working for the UI to be working properly!
        roundProgressBar.progress = percentage
    }

    override fun setNextAssignee(nextAssignee: String) {
        this.upnextTextView.text = nextAssignee
    }
}