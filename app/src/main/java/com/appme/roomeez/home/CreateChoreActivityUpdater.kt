package com.appme.roomeez.home

import com.appme.roomeez.SnackbarMessageType
import com.appme.roomeez.backend.Roomee

/**
 * Simple interface to update the CreateChoreActivity when data from the backend gets populated
 */
interface CreateChoreActivityUpdater {

    fun showProgressBar()
    fun hideProgressBar()

    /**
     * Updates the chip group for for the user to be able to select a roomee.
     * @param roomeezList - the updated list of roomeez for this house
     */
    fun updateRoomeezChipGroup(roomeezList: List<Roomee>)

    /**
     * Triggers a super call to display a snackbar message
     */
    fun displaySnackbar(message: Int, length: Int, messageType: SnackbarMessageType)

    /**
     * Creates an error around the text field
     * @param error - the error message that is to be displayed in the title text field
     */
    fun displayTitleError(error: Int)

    fun killActivity()
}