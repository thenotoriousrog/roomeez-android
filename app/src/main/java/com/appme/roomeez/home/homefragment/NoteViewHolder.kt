package com.appme.roomeez.home.homefragment

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appme.roomeez.R
import com.bumptech.glide.Glide

/**
 * Custom viewholder to properly create the Note object
 */
class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), NoteItemUpdater {

    private val noteTextView: TextView = itemView.findViewById(R.id.note_text)
    private val creatorProfileImageView: ImageView = itemView.findViewById(R.id.account_picture)
    private val creatorTextView: TextView = itemView.findViewById(R.id.creator_name)

    override fun setNoteText(note: String) {
        val editedNote = "\"" + note + "\"" // want quotes before and after the note!
        this.noteTextView.text = editedNote
    }

    override fun setCreatorProfileImage(url: String?) {
        Glide.with(itemView.context)
            .load(url)
            .error(R.drawable.ic_account_circle)
            .placeholder(R.drawable.ic_account_circle)
            .into(creatorProfileImageView)
    }

    override fun setAuthorName(author: String) {
        this.creatorTextView.text = author
    }
}