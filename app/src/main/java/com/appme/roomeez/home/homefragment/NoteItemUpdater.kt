package com.appme.roomeez.home.homefragment

/**
 * Used to update a proper note item
 */
interface NoteItemUpdater {

    /**
     * Sets the main text for the note
     * @param note the note that we want to present to the rest of the house
     */
    fun setNoteText(note: String)

    /**
     * Sets the profile picture of the image
     * @param url - the url of the user's profile image. Default image if null
     */
    fun setCreatorProfileImage(url: String?)

    /**
     * Sets name of the author
     * @param author - the author that created this note
     */
    fun setAuthorName(author: String)

}