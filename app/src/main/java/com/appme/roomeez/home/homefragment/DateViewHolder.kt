package com.appme.roomeez.home.homefragment

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appme.roomeez.R

/**
 * Custom view holder for this date object
 */
class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), DateItemUpdater {

    private val currentDateTextView: TextView = itemView.findViewById(R.id.current_date)

    override fun setCurrentDate(currentDate: String) {
        this.currentDateTextView.text = currentDate
    }
}