package com.appme.roomeez.home

import com.appme.roomeez.R
import com.appme.roomeez.SnackbarMessageType
import com.google.android.material.snackbar.Snackbar

class CreateChoreActivityPresenter(private val updater: CreateChoreActivityUpdater) {

    private val TAG = "CreateChorePresenter"

    fun retrieveRoomeez() {
        updater.showProgressBar()
        // TODO: need to update the list of roomeez, populate it, and then update the chore activity once it's been updated

        // todo: need to set the list of roomeez in the activity now!
        updater.hideProgressBar()
    }

    /**
     * Attempts to create a chore and apply it to the database after validating required data!
     * Most if not all of these items must not be null
     */
    fun createChoreButtonClicked(title: String?, description: String?,
                                 cycleSelected: String?, selectedRoomeeName: String?) {

        if(selectedRoomeeName.isNullOrBlank()) {
            updater.displaySnackbar(R.string.no_roomee_selected, Snackbar.LENGTH_LONG, SnackbarMessageType.PRIORITY)
            return
        }

        if(title.isNullOrBlank()) {
            updater.displayTitleError(R.string.title_empty)
            return
        }

        if(cycleSelected.isNullOrBlank() || cycleSelected.equals("Select chore cycle", true)) {
            updater.displaySnackbar(R.string.cycle_not_selected, Snackbar.LENGTH_LONG, SnackbarMessageType.PRIORITY)
            return
        }

        // TODO: Chore must be created and added to database now!!

        updater.killActivity()
    }

}