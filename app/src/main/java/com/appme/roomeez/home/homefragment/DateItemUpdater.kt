package com.appme.roomeez.home.homefragment

/**
 * Used to update a date object in the UI
 */
interface DateItemUpdater {

    /**
     * @param currentDate - the date, mostly spelled out in words, for today
     */
    fun setCurrentDate(currentDate: String)
}