package com.appme.roomeez

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar

/**
 * This activity is the base activity that all other activities implement.
 * Used specifically for actions that must be shared across all activities i.e. displaying a snackbar message
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
    }

    /**
     * Displays a snackbar message to the user
     * @param message - the message from a string resource i.e. R.string.some_string
     * @param length - the length of the message i.e. Snackbar.LENGTH_SHORT, Snackbar.LENGTH_LONG, or Snackbar.LENGTH_INDEFINITE
     * @param messageType - the type of snackbar message that we want to display to the user
     */
    fun displaySnackbarMessage(message: Int, length: Int, messageType: SnackbarMessageType) {
        val snackbar = Snackbar.make(getSnackbarContainerView(), message, length)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(ContextCompat.getColor(this, messageType.color))
        snackbar.show()
    }

    /**
     * Displays a toast message. Useful if a snackbar is not possible
     * @param message - the message to display via a string resource
     * @param length - the length that the toast message should be displayed for
     */
    fun displayToastMessage(message: Int, length: Int) {
        Toast.makeText(this, message, length).show()
    }

    /**
     * Gets the layout from the activity extending the base activity
     */
    abstract fun getLayoutResId(): Int

    /**
     * Grabs the view necessary for a snackbar message to be displayed to
     */
    abstract fun getSnackbarContainerView(): View
}